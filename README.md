This is a lightweight plugin that adds a custom post type to display custom icons in your theme.

## Setup

If using Advanced Custom Fields, you can use the relationship field to quickly add fields where needed. The plugin adds the post type `e29_custom_icon`.

Add each image as a post and use the featured image to save the icon.

![example](examples/01.jpg)

Register your fields with ACF

![example](examples/02.jpg)

Select your icon on the page and you can use PHP to access it where needed.

![example](examples/03.jpg)

Example:
```
<?php
$icon_id = get_field('icon')[0]; //post id
$image_id = get_post_thumbnail_id($icon_id); //featured image id
$icon_path = wp_get_attachment_image_src($image_id, 'thumbnail')[0]; //featured image path
?>

<span class="e29-custom-icon my-custom-element" style="--e29-custom-icon-path: url(<?php echo esc_url($icon_path); ?>);"></span>
```

Use the CSS class `e29-custom-icon` on your element, and pass in the URL path of the icon as a CSS custom property `--e29-custom-icon-path`. This will add a pseduo item to your element to display the icon. 

By default the element will use an icon size of 24px and the currentColor of the element in use. You can add CSS into your theme to customize it. CSS custom properties are supported. 

Example:
```
<style>
	.my-custom-element.e29-custom-icon {
		--e29-icon-size: 32px; /* default is 24px */
		--e29-icon-color: #FFF; /* default is currentColor */
	}
</style>
```