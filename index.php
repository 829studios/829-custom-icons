<?php
/**
* Plugin Name: 829 Custom Icons
* Plugin URI: https://829llc.com
* Description: Custom Post Type for Icons
* Version: 1.0
* Author: Chris Roberts
* Author URI: https://829llc.com
**/

defined('ABSPATH') or die();

class eight29_custom_icons {
	public function __construct() {
		$this->init();
	}

	public function cptui_register_my_cpts_e29_custom_icon() {

		/**
		 * Post Type: Custom Icons.
		 */
	
		$labels = [
			"name" => __( "Custom Icons", "twentytwentyone" ),
			"singular_name" => __( "Custom Icon", "twentytwentyone" ),
		];
	
		$args = [
			"label" => __( "Custom Icons", "twentytwentyone" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => true,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => false,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"delete_with_user" => false,
			"exclude_from_search" => true,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"can_export" => false,
			"rewrite" => [ "slug" => "e29_custom_icon", "with_front" => true ],
			"query_var" => true,
			"menu_icon" => "dashicons-smiley",
			"supports" => [ "title", "thumbnail" ],
			"show_in_graphql" => false,
		];
	
		register_post_type( "e29_custom_icon", $args );
	}

	public function init_post_types() {
		if (!post_type_exists('e29_custom_icon')) {
			add_action( 'init', [$this, 'cptui_register_my_cpts_e29_custom_icon'] );
		}
	}

	public function load_assets() {
		$plugin_path = plugin_dir_url(__FILE__);
		$plugin_css_path = $plugin_path.'/style.css';

		wp_enqueue_style('e29_custom_icons', $plugin_css_path, null, '1.0');
	}

	public function init() {
		$this->init_post_types();
		add_action('wp_enqueue_scripts', [$this, 'load_assets']);
	}
}

$eight29_custom_icons = new eight29_custom_icons();
?>